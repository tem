
CC?=gcc
CFLAGS?=-Wall -Wextra -std=gnu99 -Wpedantic -pipe -funroll-loops -march=native -O2
NAME?=e
PREFIX?=/usr/local/bin

SOURCE=main.c

all: $(NAME)

$(NAME): $(SOURCE)
	$(CC) -MD -MF $(<:.c=.d) $< -o $@ $(CFLAGS)

-include $(SOURCE:.c=.d)

install: $(NAME)
	install -s -c $< $(PREFIX)

uninstall: $(PREFIX)/$(NAME)
	rm -f $<
