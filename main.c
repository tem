/* Copyright (C) 2020-2022  Sergey Sushilin <sergeysushilin@protonmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of either:

   * the GNU General Public License as published by
     the Free Software Foundation; version 2.

   * the GNU General Public License as published by
     the Free Software Foundation; version 3.

   or both in parallel, as here.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copies of the GNU General Public License,
   version 2 and 3 along with this program;
   if not, see <https://www.gnu.org/licenses/>.  */

#define _GNU_SOURCE 1
#define _DEFAULT_SOURCE 1
#define _FORTIFY_SOURCE 3

#include <errno.h>
#include <error.h>
#include <fcntl.h>
#include <limits.h>
#include <paths.h>
#include <signal.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "system.h"
#include "wrappers.h"

enum action
  {
    ACTION_DAEMON_START,
    ACTION_DAEMON_STOP,
    ACTION_DAEMON_RESTART,
    ACTION_CLIENT_START,
    ACTION_LOAD_FILE
  };

/* Initialized in main().  */
static struct passwd *pw = NULL;
static uid_t uid = -1;
static char *emacs_directory_name = NULL;
static char *socket_name = NULL;

/* Here we do a simple check, whether socket exists,
   but emacs daemon is not running.  This can be when
   emacs daemon and its watchdog was terminated without
   allowing to clear up temporary files.  */
static inline void
remove_socket_if_not_connectable (void)
{
  /* Create new socket.  */
  int sock = socket (AF_LOCAL, SOCK_STREAM, 0);

  if (sock < 0)
    edie (errno, "socket(AF_LOCAL, SOCK_STREAM, 0)");

  const size_t socket_name_length = strlen (socket_name);
  struct sockaddr_un server;

  if (socket_name_length >= sizeof (server.sun_path))
    edie (0, "socket name is too long: %s", socket_name);

  server.sun_family = AF_LOCAL;
  memcpy (server.sun_path, socket_name, socket_name_length + 1);

  if (connect (sock, &server, sizeof (server)) < 0 && errno != ENOENT)
    xunlinkat (AT_FDCWD, socket_name);
  else
    shutdown (sock, SHUT_RDWR);
}

static inline __warn_unused_result bool
emacs_daemon_is_running (void)
{
  struct stat sb;

  if (unlikely (stat (socket_name, &sb) != 0))
    {
      if (unlikely (errno != ENOENT))
        edie (errno, "stat(%s)", socket_name);

      return false;
    }

  if (unlikely (!S_ISSOCK (sb.st_mode)))
    edie (ENOTSOCK, "%s", socket_name);

  /* There is a socket in our directory,
     but this socket is not owned by us.  */
  if (unlikely (sb.st_uid != uid))
    die (EXIT_FAILURE, "the socket does not belong to us");

  return likely (faccessat (AT_FDCWD, socket_name, X_OK, AT_EACCESS) == 0);
}

static inline __returns_nonnull __warn_unused_result char *
get_alternate_editor (void)
{
  char *alternate_editor = getenv ("ALTERNATE_EDITOR");

  return alternate_editor != NULL ? alternate_editor : (char *) "emacs";
}

static inline __malloc __nonnull ((1, 3)) __returns_nonnull __warn_unused_result __force_inline char *
catenate (const char *string1, char delimer, const char *string2)
{
  size_t length1 = strlen (string1), length2 = strlen (string2);
  char *s = xmalloc (length1 + 1 + length2 + 1);

  memcpy (&s[0], string1, length1);
  s[length1] = delimer;
  memcpy (&s[length1 + 1], string2, length2);
  s[length1 + 1 + length2] = '\0';
  return s;
}

static inline __malloc __returns_nonnull __warn_unused_result char **
get_required_environ (void)
{
  /* Actually almost all may-be-used by emacs environ variables.  */
  const char *names[] =
    {
      "ALTERNATE_EDITOR",
      "APPDATA",
      "CMDPROXY",
      "COLORTERM",
      "COLUMNS",
      "COMSPEC",
      "DBUS_SESSION_BUS_ADDRESS",
      "DISPLAY",
      "EMACSCLIENT_TRAMP",
      "EMACSCOLORS",
      "EMACSDATA",
      "EMACSLOADPATH",
      "EMACSTEST",
      "EMACS_SERVER_FILE",
      "EMACS_SOCKET_NAME",
      "HOME",
      "INSIDE_EMACS",
      "LANG",
      "LC_ADDRESS",
      "LC_ALL",
      "LC_COLLATE",
      "LC_CTYPE",
      "LC_IDENTIFICATION",
      "LC_MEASUREMENT",
      "LC_MESSAGES",
      "LC_MONETARY",
      "LC_NAME",
      "LC_NUMERIC",
      "LC_PAPER",
      "LC_TELEPHONE",
      "LC_TIME",
      "LINES",
      "LISP_PRELOADED",
      "LOGNAME",
      "MAIL",
      "NAME",
      "PAGER",
      "PATH",
      "PWD",
      "SHELL",
      "SUSPEND",
      "TEMP",
      "TERM",
      "TMP",
      "TMPDIR",
      "TZ",
      "USER",
      "USERNAME",
      "WAYLAND_DISPLAY",
      "XDG_CONFIG_HOME",
      "XDG_RUNTIME_DIR",
    };
  size_t envn = 0;
  char **envp = xmallocarray (countof (names) + 1, sizeof (char *));

  for (size_t i = 0; i < countof (names); i++)
    {
      const char *name = names[i];
      const char *value = getenv (name);

      if (value != NULL)
        envp[envn++] = catenate (name, '=', value);
    }

  envp[envn] = NULL;
  return xreallocarray (envp, envn + 1, sizeof (char *));
}

/* Used when starting daemon to make sure
   that we did not get in infinity waiting.  */
static __noreturn void
sigaction_sigchld (int signo __unused,
                   siginfo_t *restrict info __unused,
                   void *restrict context __unused)
{
  die (EXIT_FAILURE, "unexpected emacs daemon death");
}

static inline __nonnull ((3)) void
redirect_to_file (int descriptor, int directory_descriptor, const char *file, int flags, mode_t mode)
{
  /* Redirect descriptor to file at 'directory_descriptor/file'.  */
  int fd = xopenat (directory_descriptor, file, flags, mode);
  xdup2 (fd, descriptor);
  xclose (fd);
}

static inline void
create_emacs_directory (void)
{
  /* Access only for its user.  */
  xmkdir (emacs_directory_name, S_IRUSR | S_IWUSR | S_IXUSR);
}

static inline __warn_unused_result int
get_emacs_directory_descriptor (void)
{
  /* Get socket directory descriptor.  */
  return xopen (emacs_directory_name, O_DIRECTORY | O_CLOEXEC, 0);
}

static inline void
initialize_daemon (void)
{
  /* Change default directory to root.  */
  ignore_value (chdir ("/"));

  create_emacs_directory ();
  int emacs_directory_descriptor = get_emacs_directory_descriptor ();

  /* Redirect stdin to /dev/null for emacs daemon.  */
  redirect_to_file (STDIN_FILENO, -1, _PATH_DEVNULL, O_RDONLY, 0);

  /* Redirect stdout to /path/to/socket/log for emacs daemon.  */
  redirect_to_file (STDOUT_FILENO, emacs_directory_descriptor, "log",
                    O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);

  /* Redirect stderr to /path/to/socket/error for emacs daemon.  */
  redirect_to_file (STDERR_FILENO, emacs_directory_descriptor, "error",
                    O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);

  xclose (emacs_directory_descriptor);
}

static inline void
start_daemon (int argc, char **argv)
{
  struct sigaction new_sighup_action = { 0 };

  /* Actually, since we disconnect from controlling terminal, we will not
     be able to catch such signal, but for daemon sake...  */
  sigemptyset (&new_sighup_action.sa_mask);
  new_sighup_action.sa_handler = SIG_IGN;
  sigaction (SIGHUP, &new_sighup_action, NULL);

  /* Start daemon.  */
  pid_t daemonpid = xfork ();

  if (daemonpid != 0)
    {
      if (program_exited_unsuccessfully (wait_program_termination (daemonpid)))
        die (EXIT_FAILURE, "failed to start daemon");

      return;
    }

  /* Become session leader.  */
  setsid ();

  /* Start watchdog.  */
  pid_t watchdogpid = xfork ();

  /* Initialize sigaction for SIGCHLD.  */
  struct sigaction new_sigchld_action = { 0 };

  sigemptyset (&new_sigchld_action.sa_mask);
  new_sigchld_action.sa_flags = SA_NOCLDSTOP | SA_NOCLDWAIT | SA_SIGINFO;
  new_sigchld_action.sa_sigaction = sigaction_sigchld;

  /* Set up sigaction for SIGCHLD to find out if someone of our children
     unexpectedly die.  */
  struct sigaction old_sigchld_action;
  sigaction (SIGCHLD, &new_sigchld_action, &old_sigchld_action);

  if (watchdogpid != 0)
    {
      /* NOTE: this will not degradate to a forever cycle since we set
         SIGCHLD handler above to die if the daemon failed.  */
      while (!emacs_daemon_is_running ())
        /* Magic value (40000 microseconds == 0.04 seconds) selected
           experimentally for two purpuses:

           1) to do not make user wait too long after daemon actually
              started but we still sleeping,
           2) to reduce count of faccessat() (in emacs_daemon_is_running()
              function) and usleep() syscalls.  */
        usleep (40000);

      /* There is nothing to do more.  */
      exit (EXIT_SUCCESS);
    }

  /* Restore default behaviour.  */
  sigaction (SIGCHLD, &old_sigchld_action, NULL);

  initialize_daemon ();

  /* Start emacs daemon.  */
  pid_t emacspid = xfork ();

  if (emacspid != 0)
    {
      if (unlikely (program_exited_unsuccessfully (wait_program_termination (emacspid))))
        die (EXIT_FAILURE, "emacs daemon failed");

      exit (EXIT_SUCCESS);
    }

  int i = 1;
  int c = 0;
  char **v = xmallocarray (argc - 1 + 3, sizeof (*v));

  v[c++] = "emacs";
  v[c++] = catenate ("--fg-daemon", '=', socket_name);

  while (i < argc)
    v[c++] = argv[i++];

  v[c] = NULL;

  /* 'That's all, folks!' */
  xexecvpe (v[0], v, get_required_environ ());
}

static struct termios attrs[3];
/* Sometimes emacsclient dies unexpectively and do not reset attributes
   back, so handle this case by ourselves.  */
static void
restore_attributes (void)
{
  /* Restore std{in,out,err} attributes.  */
  xtcsetattr (STDIN_FILENO, TCSAFLUSH, &attrs[0]);
  xtcsetattr (STDOUT_FILENO, TCSAFLUSH, &attrs[1]);
  xtcsetattr (STDERR_FILENO, TCSAFLUSH, &attrs[2]);
}

static void
enstore_attributes (void)
{
  /* Enstore std{in,out,err} attributes.  */
  xtcgetattr (STDIN_FILENO, &attrs[0]);
  xtcgetattr (STDOUT_FILENO, &attrs[1]);
  xtcgetattr (STDERR_FILENO, &attrs[2]);

  xatexit (restore_attributes);
}

static inline int
start_client (int argc, char **argv)
{
  pid_t pid = xfork ();

  if (pid != 0)
    return program_exited_successfully (wait_program_termination (pid)) ? EXIT_SUCCESS : EXIT_FAILURE;

  int i = 1;
  int c = 0;
  char **v = xmallocarray (argc - 1 + 6, sizeof (*v));

  v[c++] = "emacsclient";
  v[c++] = "--tty";
  v[c++] = "--quiet";
  v[c++] = catenate ("--socket-name", '=', socket_name);
  v[c++] = catenate ("--alternate-editor", '=', get_alternate_editor ());

  /* Copy the rest arguments.  */
  while (i < argc)
    v[c++] = argv[i++];

  v[c] = NULL;

  enstore_attributes ();

  xexecvpe (v[0], v, get_required_environ ());
}

/* Called only on daemon stop.  */
static inline void
cleanup_emacs_directory (void)
{
  int emacs_directory_descriptor = get_emacs_directory_descriptor ();

  xunlinkat (emacs_directory_descriptor, "log");
  xunlinkat (emacs_directory_descriptor, "error");
  xrmdir (emacs_directory_name);
  xclose (emacs_directory_descriptor);
}

static void
stop_daemon (void)
{
  pid_t pid = xfork ();

  if (pid == 0)
    {
      char *v[5];

      v[0] = "emacsclient";
      v[1] = catenate ("--socket-name", '=', socket_name);
      v[2] = "--eval";
      v[3] = "(kill-emacs)";
      v[4] = NULL;

      xexecvpe (v[0], v, get_required_environ ());
    }

  if (unlikely (program_exited_unsuccessfully (wait_program_termination (pid))))
    {
      if (!emacs_daemon_is_running ())
        cleanup_emacs_directory ();

      die (EXIT_FAILURE, "failed to stop emacs daemon");
    }

  cleanup_emacs_directory ();
}

static inline void
restart_daemon (int argc, char **argv)
{
  stop_daemon ();
  start_daemon (argc, argv);
}

static inline void
load_file (int argc, char **argv)
{
  pid_t pid = xfork ();

  if (pid != 0)
    {
      if (unlikely (program_exited_unsuccessfully (wait_program_termination (pid))))
        die (EXIT_FAILURE, "failed to load file in emacs daemon");

      return;
    }

  int c = 0;
  char **v = xmallocarray (argc - 1 + 4 + 1, sizeof (char *));
  v[c++] = "emacsclient";
  v[c++] = catenate ("--socket-name", '=', socket_name);
  v[c++] = "--eval";

  for (int i = 1; i < argc; i++)
    {
      const char load_command[] = "(load-file \"%s\")";
      char *command = xmalloc (sizeof (load_command) - 2 - 1 + strlen (argv[i]) + 1);

      xsprintf (command, load_command, argv[i]);
      v[c++] = command;
    }

  v[c] = NULL;

  xexecvpe (v[0], v, get_required_environ ());
}

static inline __noreturn void
usage (const char *argv0, int status)
{
  fprintf (status == EXIT_SUCCESS ? stdout : stderr, "\
Usage: %s [OPTION] FILE...\n\
Tiny Emacs Manager.\n\
Every FILE can be either just a FILENAME or [+LINE[:COLUMN]] FILENAME.\n\
(Except when -load option specified).\n\
\n\
The following OPTIONS are accepted:\n\
  -startd                 Start daemon, arguments passed to started daemon.\n\
  -stopd                  Stop daemon, other arguments ignored.\n\
  -restartd               Restart daemon, arguments passed to started daemon.\n\
  -load                   Load files specified by [FILE...].\n\
  -help                   Print this usage information message.\n\
  -version                Print version.\n\
", argv0);

  exit (status);
}

static inline __noreturn void
version (void)
{
  puts ("Tiny Emacs Manager 1.0");
  exit (EXIT_SUCCESS);
}

static inline void
die_if_tty_is_not_connectable (void)
{
  /* Do the same emacs does in init_tty() to check whether
     we are able to connect to a tty.  */

#if !defined(O_IGNORE_CTTY)
# define O_IGNORE_CTTY 0
#endif

  const char *name = xttyname (STDIN_FILENO);
  int fd = openat (AT_FDCWD, name, O_RDWR | O_NOCTTY | O_IGNORE_CTTY, 0);

  if (fd < 0)
    edie (errno, "%s", name);
  else
    xclose (fd);
}

static inline enum action
parse_options (int *restrict argcp, char ***restrict argvp)
{
  int argc = *argcp;
  char **argv = *argvp;
  enum action action = ACTION_CLIENT_START;
  char *arg = argv[1];

  if (unlikely (arg[0] == '-' && arg[1] != '\0'))
    {
      arg++;
      argc--;
      argv++;

      if (streq (arg, "help"))
        usage ((*argvp)[0], EXIT_SUCCESS);
      else if (streq (arg, "version"))
        version ();
      else if (streq (arg, "startd"))
        action = ACTION_DAEMON_START;
      else if (streq (arg, "stopd"))
        action = ACTION_DAEMON_STOP;
      else if (streq (arg, "restartd"))
        action = ACTION_DAEMON_RESTART;
      else if (streq (arg, "load"))
        action = ACTION_LOAD_FILE;
      else
        {
          fprintf (stderr, "unknown option %s\n", arg - 1);
          usage ((*argvp)[0], EXIT_FAILURE);
        }
    }

  *argcp = argc;
  *argvp = argv;
  return action;
}

static inline void
initialize_emacs_directory_name (void)
{
  const char emacs_directory_name_format[] = "%s/.emacs-server";

  emacs_directory_name = xmalloc (sizeof (emacs_directory_name_format) - 1 - 2 + strlen (pw->pw_dir) + 1);
  xsprintf (emacs_directory_name, emacs_directory_name_format, pw->pw_dir);
}

static inline void
initialize_socket_name (void)
{
  const char socket_name_format[] = "%s/.emacs-server/socket";

  socket_name = xmalloc (sizeof (socket_name_format) - 1 - 2 + strlen (pw->pw_dir) + 1);
  xsprintf (socket_name, socket_name_format, pw->pw_dir);
}

int
main (int argc, char **argv)
{
  int status = EXIT_SUCCESS;
  enum action action = ACTION_CLIENT_START;

  die_if_tty_is_not_connectable ();
  uid = xgetuid ();
  pw = xgetpwuid (uid);

  initialize_emacs_directory_name ();
  initialize_socket_name ();

  if (argc > 1)
    action = parse_options (&argc, &argv);

  remove_socket_if_not_connectable ();

  if (unlikely (action != ACTION_DAEMON_START && !emacs_daemon_is_running ()))
    {
      /* Little feature to allow user do not specially care about 'e -startd'
         when he just want to edit file by 'e filename'.  */
      if (action == ACTION_CLIENT_START)
        start_daemon (0, NULL);
      else
        die (EXIT_FAILURE, "emacs daemon is not running");
    }

  switch (action)
    {
    case ACTION_DAEMON_START:
      start_daemon (argc, argv);
      break;
    case ACTION_DAEMON_STOP:
      stop_daemon ();
      break;
    case ACTION_DAEMON_RESTART:
      restart_daemon (argc, argv);
      break;
    case ACTION_CLIENT_START:
      status = start_client (argc, argv);
      break;
    case ACTION_LOAD_FILE:
      if (argc < 1)
        die (EXIT_FAILURE, "at least one file name to load required");

      load_file (argc, argv);
      break;
    }

  return status;
}
