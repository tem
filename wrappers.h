/* Copyright (C) 2020-2022  Sergey Sushilin <sergeysushilin@protonmail.com>

   This program is free software; you can redistribute it and/or modify
   it under the terms of either:

   * the GNU General Public License as published by
     the Free Software Foundation; version 2.

   * the GNU General Public License as published by
     the Free Software Foundation; version 3.

   or both in parallel, as here.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copies of the GNU General Public License,
   version 2 and 3 along with this program;
   if not, see <https://www.gnu.org/licenses/>.  */

#ifndef _WRAPPERS_H
#define _WRAPPERS_H 1

#define _GNU_SOURCE 1

#include <errno.h>
#include <error.h>
#include <fcntl.h>
#include <limits.h>
#include <pwd.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <unistd.h>

#include "system.h"

#if !__GCC_PREREQ (5, 0)
/* https://stackoverflow.com/a/1815371 */
static __warn_unused_result __force_inline __nonnull ((3)) bool
__builtin_mul_overflow (size_t a, size_t b, size_t *c)
{
# define SIZE_BIT (sizeof (size_t) * CHAR_BIT / 2)
# define HI(x) (x >> SIZE_BIT)
# define LO(x) ((((size_t)1 << SIZE_BIT) - 1) & x)

  size_t s0, s1, s2, s3, x, result, carry;

  x = LO (a) * LO (b);
  s0 = LO (x);

  x = HI (a) * LO (b) + HI (x);
  s1 = LO (x);
  s2 = HI (x);

  x = s1 + LO (a) * HI (b);
  s1 = LO (x);

  x = s2 + HI (a) * HI (b) + HI (x);
  s2 = LO (x);
  s3 = HI (x);

  result = s1 << SIZE_BIT | s0;
  carry = s3 << SIZE_BIT | s2;

  *c = result;

  return carry != 0;
}
# endif

#define mul_overflow(n, m, s) __builtin_mul_overflow (n, m, s)

#define edie(e, ...) (error (EXIT_FAILURE, e, __VA_ARGS__), assume (false))
#define die(c, s)    ((void) BARF_IF_FAIL ((c) != 0), fprintf (stderr, "%s\n", s), exit (c), assume (false))

static inline bool program_exited_successfully (int status) __warn_unused_result __const;
static inline bool
program_exited_successfully (int status)
{
  return WIFEXITED (status) && WEXITSTATUS (status) == EXIT_SUCCESS;
}
static inline bool program_exited_unsuccessfully (int status) __warn_unused_result __const;
static inline bool
program_exited_unsuccessfully (int status)
{
  return !WIFEXITED (status) || WEXITSTATUS (status) != EXIT_SUCCESS;
}
static inline bool program_terminated (int status) __warn_unused_result __const;
static inline bool
program_terminated (int status)
{
  return WIFEXITED (status) || (WIFSIGNALED (status) && !WIFSTOPPED (status) && !WIFCONTINUED (status));
}

static inline int xopenat (int dd, const char *file, int oflags, int cflags) __nonnull ((2));
static inline int
xopenat (int dd, const char *file, int oflags, int cflags)
{
  int fd = openat (dd, file, oflags, cflags);
  if (fd < 0)
    edie (errno, "openat(%s)", file);
  return fd;
}

static inline int xopen (const char *file, int oflags, int cflags) __nonnull ((1));
static inline int
xopen (const char *file, int oflags, int cflags)
{
  int fd = open (file, oflags, cflags);

  if (fd < 0)
    edie (errno, "open(%s)", file);

  return fd;
}

static inline void xdup2 (int oldfd, int newfd);
static inline void
xdup2 (int oldfd, int newfd)
{
  if (dup2 (oldfd, newfd) < 0)
    edie (errno, "dup2(%d, %d)", oldfd, newfd);
}

static inline void xclose (int fd);
static inline void
xclose (int fd)
{
  if (close (fd) < 0)
    edie (errno, "close(%d)", fd);
}

static inline void
xpipe (int pipes[2])
{
  if (pipe (pipes) != 0)
    edie (errno, "pipe()");
}

static inline size_t full_write (int fd, const void *buf, size_t len) __nonnull ((2));
static inline size_t
full_write (int fd, const void *buf, size_t len)
{
  const char *s = buf;
  size_t n = len;

  if (len == 0)
    {
      errno = EINVAL;
      return 0;
    }

  do
    {
      int saved_errno = errno;
      ssize_t ret = write (fd, s, n);

      if (ret < 0)
        {
          if (errno == EINTR || errno == EWOULDBLOCK || errno == EAGAIN)
            {
              errno = saved_errno;
              continue;
            }
          break;
        }

      if (ret == 0)
        {
          /* According to gnulib note.
             Some buggy drivers return 0 when one tries
             to write beyond a device's end.
             (Example: Linux 1.2.13 on /dev/fd0.)
             Set errno to ENOSPC so they get a sensible
             diagnostic.  */
          errno = ENOSPC;
          break;
        }

      s += ret;
      n -= ret;
    }
  while (n != 0);

  return len - n;
}

static inline size_t full_read (int fd, void *buf, size_t len) __nonnull ((2));
static inline size_t
full_read (int fd, void *buf, size_t len)
{
  char *s = buf;
  size_t n = len;

  if (len == 0)
    {
      errno = EINVAL;
      return 0;
    }

  do
    {
      int saved_errno = errno;
      ssize_t ret = read (fd, s, n);

      if (ret < 0)
        {
          if (errno == EWOULDBLOCK || (errno != EINTR && errno != EAGAIN))
            {
              break;
            }
          else
            {
              errno = saved_errno;
              continue;
            }
        }

      if (ret == 0)
        break;

      s += ret;
      n -= ret;
    }
  while (n != 0);

  return len - n;
}

static inline size_t xread (int fd, void *buf, size_t len);
static inline size_t
xread (int fd, void *buf, size_t len)
{
  size_t r = full_read (fd, buf, len);

  if (r == 0)
    edie (errno, "read(%d, %lu)", fd, (unsigned long int) len);

  return r;
}

static inline size_t xwrite (int fd, const void *buf, size_t len);
static inline size_t
xwrite (int fd, const void *buf, size_t len)
{
  size_t r = full_write (fd, buf, len);

  if (r == 0)
    edie (errno, "write(%d, %lu)", fd, (unsigned long int)  len);

  return r;
}

static inline const char *xttyname (int fd) __returns_nonnull;
static inline const char *
xttyname (int fd)
{
  const char *name = ttyname (fd);

  if (name == NULL)
    edie (errno, "ttyname(%d)", fd);

  return name;
}

static inline void *xmalloc (size_t s) __malloc __alloc_size ((1)) __returns_nonnull __warn_unused_result;
static inline void *
xmalloc (size_t s)
{
  void *pointer = malloc (s);

  if (unlikely (pointer == NULL))
    edie (errno, "malloc()");

  return pointer;
}

static inline void *xmallocarray (size_t n, size_t m) __malloc __alloc_size ((1, 2)) __returns_nonnull __warn_unused_result;
static inline void *
xmallocarray (size_t n, size_t m)
{
  size_t s;

  if (unlikely (mul_overflow (n, m, &s)))
    edie (ENOMEM, "malloc()");

  return xmalloc (s);
}

static inline void *xrealloc (void *pointer, size_t s) __malloc __nonnull ((1)) __alloc_size ((2)) __returns_nonnull __warn_unused_result;
static inline void *
xrealloc (void *pointer, size_t s)
{
  pointer = realloc (pointer, s);

  if (unlikely (pointer == NULL))
    edie (errno, "realloc()");

  return pointer;
}

static inline void *xreallocarray (void *pointer, size_t n, size_t m) __malloc __nonnull ((1)) __alloc_size ((2, 3)) __returns_nonnull __warn_unused_result;
static inline void *
xreallocarray (void *pointer, size_t n, size_t m)
{
  size_t s;

  if (unlikely (mul_overflow (n, m, &s)))
    edie (ENOMEM, "realloc()");

  pointer = realloc (pointer, s);

  if (unlikely (pointer == NULL))
    edie (errno, "realloc()");

  return pointer;
}

static inline pid_t xfork (void) __warn_unused_result;
static inline pid_t
xfork (void)
{
  pid_t pid = fork ();

  if (unlikely (pid < 0))
    edie (errno, "fork()");

  return pid;
}

static inline pid_t xwaitpid (pid_t pid, int *status, int flags) __nonnull ((2));
static inline pid_t
xwaitpid (pid_t pid, int *status, int flags)
{
  pid_t r;

  while (unlikely ((r = waitpid (pid, status, flags)) < 0))
    if (unlikely (errno != EINTR))
      edie (errno, "waitpid()");

  return r;
}

static inline void xexecvpe (char *file, char **argv, char **envp) __noreturn __nonnull ((1, 2, 3));
static inline void
xexecvpe (char *file, char **argv, char **envp)
{
  execvpe (file, argv, envp);
  /* Unreachable on success.  */
  edie (errno, "execvpe(\"%s\")", file);
}

static inline sighandler_t
xsignal (int signo, sighandler_t handler)
{
  sighandler_t previous = signal (signo, handler);

  if (previous == SIG_ERR)
    edie (errno, "signal(%d)", signo);

  return previous;
}

static inline void
xraise (int signo)
{
  if (unlikely (raise (signo) != 0))
    edie (errno, "raise(%d)", signo);
}

static inline void
xkill (pid_t pid, int signo)
{
  if (unlikely (kill (pid, signo) != 0))
    edie (errno, "kill(%d, %d)", pid, signo);
}

static inline void xchmod (const char *file_name, mode_t mode) __nonnull ((1));
static inline void
xchmod (const char *file_name, mode_t mode)
{
  if (unlikely (chmod (file_name, mode) != 0))
    edie (errno, "chmod(%s, %o)", file_name, mode);
}

static inline void xmkdir (const char *directory_name, mode_t mode) __nonnull ((1));
static inline void
xmkdir (const char *directory_name, mode_t mode)
{
  if (unlikely (mode & ~07777))
    edie (EINVAL, "mkdir(%s, %o)", directory_name, mode);

  if (unlikely (mkdir (directory_name, mode) != 0))
    {
      struct stat sb;

      if (unlikely (errno != EEXIST))
        edie (errno, "mkdir(%s, %o)", directory_name, mode);
      /* Do not die if there is directory with given name.  */

      if (unlikely (stat (directory_name, &sb) != 0))
        edie (errno, "stat(%s)", directory_name);

      if (unlikely (!S_ISDIR (sb.st_mode)))
        edie (ENOTDIR, "%s", directory_name);

      /* If directory exists make sure that it has required mode.  */
      if (unlikely ((sb.st_mode & 07777) != mode))
        xchmod (directory_name, mode);
    }
}

static inline void xunlinkat (int dd, const char *path) __nonnull ((2));
static inline void
xunlinkat (int dd, const char *path)
{
  if (unlinkat (dd, path, 0) < 0 && errno != ENOENT)
    edie (errno, "unlinkat(%s)", path);
}

static inline void xrmdir (const char *directory_name) __nonnull ((1));
static inline void
xrmdir (const char *directory_name)
{
  if (rmdir (directory_name) != 0 && errno != ENOENT)
    edie (errno, "rmdir()");
}

static inline int xsprintf (char *s, const char *fmt, ...) __nonnull ((1, 2)) __format_printf (2, 3);
static inline int
xsprintf (char *s, const char *fmt, ...)
{
  int n;
  va_list ap;

  va_start (ap, fmt);
  n = vsprintf (s, fmt, ap);
  va_end (ap);

  if (n < 0)
    edie (errno, "sprintf()");

  return n;
}

#if 0
static inline int xasprintf (char **s, const char *fmt, ...) __nonnull ((1, 2)) __format_printf (2, 3);
static inline int
xasprintf (char **s, const char *fmt, ...)
{
  int n;
  va_list ap, ap2;

  va_start (ap, fmt);
  va_copy (ap2, ap);

  if (unlikely ((n = vsnprintf (NULL, 0, fmt, ap)) < 0))
    edie (errno, "vsnprintf()");

  va_end (ap);

  if (*s == NULL)
    *s = xmallocarray (n + 1, sizeof (char));
  else
    *s = xreallocarray (*s, n + 1, sizeof (char));

  if (unlikely ((n = vsnprintf (*s, n + 1, fmt, ap2)) < 0))
    edie (errno, "vsnprintf()");

  va_end (ap2);

  return n;
}

static inline void xastrcat (char **d, const char *s) __nonnull ((1, 2));
static inline void
xastrcat (char **d, const char *s)
{
  size_t s_length = strlen (s);
  size_t d_length = strlen (*d);
  *d = xreallocarray (*d, d_length + s_length + 1, sizeof (char));
  strcpy (*d + d_length, s);
}
#endif

static inline uid_t xgetuid (void) __warn_unused_result;
static inline uid_t
xgetuid (void)
{
  uid_t uid;
  int saved_errno = errno;

  errno = 0;

  if (unlikely ((uid = getuid ()) == (uid_t) -1 && errno != 0))
    edie (errno, "getuid()");

  errno = saved_errno;

  return uid;
}

static inline struct passwd *xgetpwuid (uid_t uid)
  __warn_unused_result __malloc;
static inline struct passwd *
xgetpwuid (uid_t uid)
{
  int s;
  size_t size = 64;
  char *p = xmalloc (sizeof (struct passwd) + size);
  struct passwd *result = NULL;

  do
    {
      s = getpwuid_r (uid, (struct passwd *) p, p + sizeof (struct passwd), size, &result);

      if (s == EINTR)
        continue; /* Try again.  */

      if (s == ERANGE)
        {
          if (mul_overflow (2, size, &size))
            edie (ENOMEM, "getpwuid_r(%lu)", (unsigned long int) uid);

          p = xrealloc (p, sizeof (struct passwd) + size);
          continue;
        }

      /* We either got an error, or we succeeded and the
         returned name fit in the buffer.  */
      break;
    }
  while (true);

  if (s != 0)
    {
      free (p);
      result = NULL;
      edie (s, "getpwuid_r(%lu)", (unsigned long int) uid);
    }
  else if (result == NULL)
    {
      free (p);
      edie (0, "no matching password record was found for user with uid %lu",
            (unsigned long int) uid);
    }

  return result;
}

static inline char *xgetwd (void) __returns_nonnull __warn_unused_result __malloc;
static inline char *
xgetwd (void)
{
  size_t size = PATH_MAX;
  char *buffer = xmalloc (PATH_MAX);
  char *p;

  while ((p = getcwd (buffer, size)) == NULL)
    if (errno == ERANGE)
      buffer = xrealloc (buffer, size *= 2);
    else
      edie (errno, "getcwd()");

  return xrealloc (p, strlen (p) + 1);
}

static inline void xtcgetattr (int fd, struct termios *termiosp) __nonnull ((2));
static inline void
xtcgetattr (int fd, struct termios *termiosp)
{
  if (tcgetattr (fd, termiosp) < 0)
    edie (errno, "tcgetattr()");
}

static inline void xtcsetattr (int fd, int optional_actions, const struct termios *termiosp) __nonnull ((3));
static inline void
xtcsetattr (int fd, int optional_actions, const struct termios *termiosp)
{
  if (tcsetattr (fd, optional_actions, termiosp) < 0)
    edie (errno, "tcsetattr()");
}

static inline void xatexit (void (*f) (void)) __nonnull ((1));
static inline void
xatexit (void (*f) (void))
{
  if (atexit (f) != 0)
    edie (errno, "atexit()");
}

static inline int program_get_status (pid_t pid, int flags) __warn_unused_result;
static inline int
program_get_status (pid_t pid, int flags)
{
  int status;

  xwaitpid (pid, &status, flags);

  return status;
}

static inline int wait_program_termination (pid_t pid) __warn_unused_result;
static inline int
wait_program_termination (pid_t pid)
{
  int status;

  do
    xwaitpid (pid, &status, 0);
  while (unlikely (!program_terminated (status)));

  return status;
}

static inline bool program_is_executing (pid_t pid) __warn_unused_result;
static inline bool
program_is_executing (pid_t pid)
{
  /*TODO*/
  int status;

  xwaitpid (pid, &status, WNOHANG);
  return (!program_terminated (status));
}

poison (ttyname);
poison (malloc calloc realloc fork kill raise execvpe waitpid);
poison (chmod chown fchown mkdir rmdir getuid geteuid getgid setuid seteuid);
poison (sprintf snprintf asprintf);
poison (tcgetattr tcsetattr atexit read write lseek unlink nice);

#endif
